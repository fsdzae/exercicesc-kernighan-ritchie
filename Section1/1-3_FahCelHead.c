#include <stdio.h>

void main()
{
    
    float fah, celsius;
    int lower, upper, step;

    lower = 0;
    upper = 300;
    step = 20;

    fah = lower;

    printf("Fahrenheit -> Celsius\n");
    while(fah <= upper){
        celsius = (5.0/9.0) * (fah - 32.0);
        printf("%3.0f %6.0f\n", fah, celsius);
        fah = fah + step;
    }
}