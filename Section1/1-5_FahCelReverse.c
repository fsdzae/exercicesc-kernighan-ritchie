#include <stdio.h>

#define UPPER 300
#define LOWER 0
#define STEP 20

void main()
{
    int fah;
    
    printf("Fahrenheit -> Celsius\n");    
    
    for(fah = UPPER; fah >= LOWER; fah = fah - STEP)
        printf("%3d %6.1f\n", fah, (5.0/9.0)*(fah-32));

}
