#include <stdio.h>

void main(){

int c, c_ws, c_tab, c_nl;

c_ws = 0;
c_tab = 0;
c_nl = 0;

while((c = getchar()) != EOF){
    if(c == '\n')
        ++c_nl;
    else if(c == '\t')
        ++c_tab;
    else if(c == ' ')
        ++c_ws;
}

printf("The new lines were %d\n", c_nl);
printf("The tabs were %d\n", c_tab);
printf("The white spaces were %d\n", c_ws);

}
