#include <stdio.h>

void main()
{

int c, ndigit[10], i, j;

for(i = 0; i < 10; ++i){
    ndigit[i] = 0;
}

while((c = getchar()) != EOF){
    if(c >= '0' && c <= '9')
        ++ndigit[c-'0'];
}

for(i = 0; i < 10; ++i){
    printf("%d", ndigit[i]);
    if(i == 10-1)
        printf("\n");
}

for(i = 0; i < 10; ++i){
    printf("%d", ndigit[i]);
        for(j = 0; j < ndigit[i]; ++j)
            printf("-");
        printf("\n");
}

}