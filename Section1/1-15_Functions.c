#include <stdio.h>

float celFah();

void main()
{
    celFah();   
}

float celFah(){

    float fah, celsius;
    int lower, upper, step;

    lower = 0;
    upper = 300;
    step = 20;

    celsius = lower;

    printf("Celsius -> Fahrenheit\n");

    while(celsius <= upper){
        fah = celsius*(9.0/5.0) + 32;
        printf("%3.0f %6.0f\n", celsius, fah);
        celsius = celsius + step;
    }

}