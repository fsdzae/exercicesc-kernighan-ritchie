#include <stdio.h>

void main(){

int c, i, j, chars[26] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
int nchars[26];

for(i = 0; i < 26; ++i){
    nchars[i] = 0;
}

while((c = getchar()) != EOF){
    for(i = 0; i < 26; i++){
        if(c == chars[i])
           ++nchars[i];
    }
}

for(i = 0; i < 26; ++i){
    printf("%c", chars[i]);
        for(j = 0; j < nchars[i]; j++){
            printf("-");
        }
    printf("\n");
}
               
}